/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#ifndef COLOG_H_
#define COLOG_H_

void colog_normal(char *fmt, ...);
void colog_poll(char *fmt, ...);

void create_log_thread(void);
void wakeup_log_thread(void);

/* 这几个函数由硬件驱动提供 */
int write_completed(void);
void write_ch(char ch);

#endif // COLOG_H_

