/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#ifndef TIMER_H_
#define TIMER_H_

typedef void *timer_t;
typedef void (*timer_fun)(void *arg);

timer_t timer_set(int timeout_ms, int interval_ms, timer_fun fun, void *arg);
int timer_destroy(timer_t timerid);

#endif

